package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String name=scanner.nextLine();
        scanner.close();
        System.out.println("Hello, Nice to meet you, "+name+"!");

    }
}
